data = []


with open("JACK5.DAT") as f:
    w = [int(x) for x in next(f).split()]
    for line in f:
        data.append([int(x) for x in line.split()])


data = sorted(data, key=lambda var: var[0])


def evaluatecmax(tasks):
    t = 0
    for x in range(tasks.__len__()):
        t = max(tasks[x][0], t) + tasks[x][1]

    return t


print(evaluatecmax(data))