dane = []


with open("NEH1.DAT") as f:
    w, h = [int(x) for x in next(f).split()]
    for line in f:
        dane.append([int(x) for x in line.split()])

z = w
m = h


def maxtime(ddane, liczba):
    poczatki = [[] for _ in range(z)]

    t = 0
    for x in range(liczba):
        for y in range(m):
            poczatki[x].append(t)
            if x == 0:
                t = max(t, t + ddane[x][y])
                if y == m-1:
                    t = poczatki[x][1]
            else:
                if y == m-1:
                    t = poczatki[x][0] + ddane[x][0]
                else:
                    t = max(t + ddane[x][y], poczatki[x-1][y+1] + ddane[x-1][y+1])
    return poczatki[liczba-1][m-1] + ddane[liczba-1][m-1]


def sortowaniedwa(tab):
    a = maxtime(tab, 2)
    tab[0], tab[1] = tab[1], tab[0]
    b = maxtime(tab, 2)
    print("tutaj ab", a ,b )
    if a < b:
        tab[0], tab[1] = tab[1], tab[0]
    return tab


def konfiguracja(dane2):

    pierwszeustawienie = 0
    final = dane2[:2].copy()

    for k in range(2, z):
        for l in range(0, k+1):

            M = final[0:k].copy()
            M.insert(l, dane2[k])
            print("L:{} k:{} M:{}".format(l, k, M ))
            print("tutaj maxtime", maxtime(M, k+1))

            if l == 0:
                poprzednimax = maxtime(M, k+1)
                pierwszeustawienie = maxtime(M, k+1)
            else:
                if maxtime(M, k+1) < poprzednimax:
                    poprzednimax = maxtime(M, k+1)
                    temp = l

        if pierwszeustawienie == poprzednimax:
            final.insert(0, dane2[k])
        else:
            final.insert(temp, dane2[k])

        print(poprzednimax, final)
        print()
    return maxtime(final, z)


posortowanedane = sorted(dane, key=sum, reverse=True)

print("DANE:", dane)
print("POSORTOWANE DANE:", posortowanedane)

o = sortowaniedwa(posortowanedane)

print("DWA PIERWSZE POSORTOWANE", o)

print(maxtime(posortowanedane[:2],2))

p = konfiguracja(o)
print("Minimalne Cmax:", p)