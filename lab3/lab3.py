data = []


def klucz(indeks): return lambda var: var[indeks]


with open("SCHRAGE1.DAT") as f:
    w = [int(x) for x in next(f).split()]
    for line in f:
        data.append([int(x) for x in line.split()])


def evaluatecmax(tasks):
    t = 0
    cmax = 0
    g = []

    while tasks or g:
        while data and min(tasks, key=klucz(0))[0] <= t:
            g.append(min(tasks, key=klucz(0)))
            tasks.remove(min(tasks, key=klucz(0)))

        if not g:
            t = min(tasks, key=lambda var: var[0])[0]
        else:
            t = t+max(g, key=klucz(2))[1]
            cmax = max(cmax, t + max(g, key=klucz(2))[2])
            g.remove(max(g, key=klucz(2)))

    return cmax


print(evaluatecmax(data))
